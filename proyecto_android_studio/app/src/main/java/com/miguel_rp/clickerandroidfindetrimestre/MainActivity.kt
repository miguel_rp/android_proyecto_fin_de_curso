package com.miguel_rp.clickerandroidfindetrimestre

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.miguel_rp.clickerandroidfindetrimestre.databinding.ActivityMainBinding

var UI_listo:Boolean = true
class MainActivity : AppCompatActivity() {
    var sistema_juego:Sistema_juego? =null
    lateinit var bindind:ActivityMainBinding;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindind =  ActivityMainBinding.inflate(layoutInflater)
        bindind.mejorarClickButton.setOnClickListener(){

        }
        bindind.mejorarOroPorSegundo.setOnClickListener(){

        }
        sistema_juego = Sistema_juego(findViewById(R.id.textViewOro),this)
        //Nota: Para un OnClickListener custom: El binding no sirve porque no registra la ubicación del objeto
        Metodos_Comunes.gestionar_boton_mina(sistema_juego!!.ganancia_por_click,findViewById(R.id.imageMina),R.drawable.gold_ore_hover,R.drawable.gold_ore,this){
Log.v("OBJETO","Primera funcion detecta onclick")
            sistema_juego!!.click()
        }

    }
}