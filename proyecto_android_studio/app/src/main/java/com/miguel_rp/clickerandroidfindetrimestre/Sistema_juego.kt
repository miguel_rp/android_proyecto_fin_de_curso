package com.miguel_rp.clickerandroidfindetrimestre

import android.app.Activity
import android.widget.TextView
import java.util.*
import kotlin.concurrent.schedule

class Sistema_juego {
    var activity:Activity? = null
    var textview_oro:TextView? = null;
    var oro_disponible: Long
    var oro_total: Long
    var ganancia_por_click: Long
    var ganancia_por_segundo: Long

    constructor(
        oro_disponible: Long,
        oro_total: Long,
        ganancia_por_click: Long,
        ganancia_por_segundo: Long,
        textview:TextView,
        activiry:Activity
    ) {
        activity = activiry
        textview_oro= textview
        this.oro_disponible = oro_disponible
        this.oro_total = oro_total
        this.ganancia_por_click = ganancia_por_click
        this.ganancia_por_segundo = ganancia_por_segundo
        tick()
    }

    constructor(textview:TextView,activiry:Activity) {
        activity = activiry
        oro_disponible = 0
        oro_total = 0
        ganancia_por_click = 1
        ganancia_por_segundo = 0
        textview_oro= textview
        tick()
    }

    fun tick() {
        Timer("schedule", true).schedule(1000) {
            activity!!.runOnUiThread {
                oro_disponible+=ganancia_por_segundo
                oro_total+=ganancia_por_segundo
                actualizar_textview()
            }
            tick()
        }

    }

    fun actualizar_textview(){
        textview_oro!!.text = (oro_disponible.toString() + " oro.")
    }

    fun click(){
        oro_disponible+=ganancia_por_click
        oro_total+=ganancia_por_click
        actualizar_textview()
    }

}