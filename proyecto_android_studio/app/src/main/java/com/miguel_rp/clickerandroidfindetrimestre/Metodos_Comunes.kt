package com.miguel_rp.clickerandroidfindetrimestre

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.media.MediaPlayer
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AnimationUtils
import android.widget.ImageButton
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.plattysoft.leonids.ParticleSystem

class Metodos_Comunes {
    companion object {

        @SuppressLint("ClickableViewAccessibility")
        public fun gestionar_boton_mina(numero_particulas:Long,
            boton_main: ImageView, drawable_hover: Int, drawable_normal: Int, activity: Activity,
            function: () -> (Unit)
        ) {
            Log.v("OBJETO","boton_declarado")
            val handleTouchEmpezar = View.OnTouchListener { v, event ->
                when (event.action) {
                    MotionEvent.ACTION_MOVE -> {
                        Log.v("OBJETO","Accion move detectada")
                        return@OnTouchListener true
                    }
                    MotionEvent.ACTION_DOWN -> {
                        Log.v("OBJETO","Accion down detectada")
                        //Toast.makeText(activity, "Entra en funciñon", Toast.LENGTH_SHORT).show()
                        if (UI_listo) {
                            boton_main.setImageDrawable(
                                ContextCompat.getDrawable(
                                    activity,
                                    drawable_hover
                                )
                            )
                            val mediaPlayerMain: MediaPlayer? = MediaPlayer.create(activity.applicationContext, R.raw.romper_boton);
                            mediaPlayerMain!!.start()
                            val ps1 = ParticleSystem(activity, 100, R.drawable.gold_ore_roto, 1000)
                            ps1.setScaleRange(0.5f, 1.0f)
                            ps1.setSpeedModuleAndAngleRange(0.07f, 0.16f, 0, 180)
                            ps1.setRotationSpeedRange(90f, 120f)
                            ps1.setAcceleration(0.00113f, 90)
                            ps1.setFadeOut(200, AccelerateInterpolator())
                            if(numero_particulas>20){
                                ps1.emit(boton_main, 20, 100)
                            }else{
                                ps1.emit(boton_main, numero_particulas.toInt(), 100)
                            }
                            boton_main.startAnimation(AnimationUtils.loadAnimation(activity.applicationContext, R.anim.agitar_arriba_abajo))
                            function()
                        }
                        return@OnTouchListener true

                    }
                    MotionEvent.ACTION_UP -> {
                        Log.v("OBJETO","Accion up detectada")
                        var dentro = false
                        //val rect: Rect = Rect(v.left, v.top, v.right, v.bottom)
                        val rect: Rect = Rect()
                        boton_main.getHitRect(rect)
                        if (UI_listo) if (rect.contains(
                                event.x.toInt(),
                                event.y.toInt()
                            )
                        ) dentro = true
                        if (dentro) {
                            //Log.v("OBJETO", "Entra  ")
                            if (UI_listo) {
                                boton_main.setImageDrawable(
                                    ContextCompat.getDrawable(
                                        activity,
                                        drawable_normal
                                    )
                                )
                            }
                        } else {
                            //Log.v("OBJETO", "NO Entra  ")
                            if (UI_listo) {
                                boton_main.setImageDrawable(
                                    ContextCompat.getDrawable(
                                        activity,
                                        drawable_normal
                                    )
                                )
                            }
                        }
                        return@OnTouchListener true
                    }

                    else -> {
                        false
                    }
                }

            }
            boton_main.setOnTouchListener(handleTouchEmpezar);
        }
    }
}